package com.dg.apps.rlp.db.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

import com.dg.apps.rlp.db.Cell;
import com.dg.apps.rlp.db.Database;
import com.dg.apps.rlp.mail.Mail;

public class LogLinesTable extends Database<Mail>
{
	private String tableName = "tbl_loglines";
	
	public LogLinesTable()
	{
		//super("/home/dglnx/data.db");
		super("C:\\Users\\david\\Desktop\\data\\data.db");
	}
	
	@Override
	public int create() throws ClassNotFoundException, SQLException
	{
		String sql = new StringBuilder()
				.append("CREATE TABLE ")
				.append(tableName)
				.append("(")
				.append("INSERT_DATETIME DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,")
				.append("UPDATE_DATETIME DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,")
				.append("EMAIL_RECV_DATETIME DATETIME NOT NULL,")
				.append("CONTENT VARCHAR(32768),")
				.append("PROCESSED INTEGER,")
				.append("PRIMARY KEY (EMAIL_RECV_DATETIME, CONTENT)")
				.append(");")
				.toString();
		
		return write(sql);
	}
	
	@Override
	public boolean exists() throws ClassNotFoundException, SQLException
	{
		String sql = new StringBuilder()
				.append("SELECT name ")
				.append("FROM sqlite_master ")
				.append("WHERE TYPE='table'")
				.append("AND name='")
				.append(tableName)
				.append("'")
				.toString();
		
		Collection<Collection<Cell<String,String>>> rows = select(sql);
		return !rows.isEmpty();
	}
	
	@Override
	public int insert(Mail input) throws ClassNotFoundException, SQLException
	{
		/* check to see if this date already exists in the unprocessed db */
		Collection<Collection<Cell<String,String>>> existing = select("SELECT * FROM " + tableName + " WHERE EMAIL_RECV_DATETIME = " + input.getSendDate().getTime());
		if (!existing.isEmpty())
		{
			System.out.println("date [" + input.getSendDate() + "] already exists, aborting insert.");
			return 0;
		}
		
		/* move forward with building the insert if the date doesnt already exist */
		StringBuilder sb = new StringBuilder()
				.append("INSERT INTO ")
				.append(tableName)
				.append("(EMAIL_RECV_DATETIME,CONTENT,PROCESSED)VALUES");
		
		List<String> lines = input.getContentByLines();
		StringBuilder values = new StringBuilder();
		for(String line : lines)
		{
			if (values.length() > 0)
			{
				values.append(",");
			}
			values.append("(").append("?,?,?)");
		}
		String sql = sb.append(values.toString()).toString();
		
		/* insert the data into the table */
		Connection connection = null;
		PreparedStatement statement = null;
		try
		{
			Class.forName(SQLLITE_JDBC_CLASS_NAME);
			connection = DriverManager.getConnection(DATABASE_URL + filePathSpec);
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			
			int index = 1;
			for (String line : lines)
			{
				statement.setTimestamp(index++, new Timestamp(input.getSendDate().getTime()));
				statement.setString(index++, line);
				statement.setBoolean(index++, input.isProcessed());
			}
			
			int result = statement.executeUpdate();
			connection.commit();
			System.out.println("wrote [" + result + "] to [" + filePathSpec + "].");
			return result;
		}
		catch(ClassNotFoundException | SQLException ex)
		{
			rollback(connection);
			throw ex;
		}
		finally
		{
			close(statement);
			close(connection);
		}
	}
}