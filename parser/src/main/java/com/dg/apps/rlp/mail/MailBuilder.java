package com.dg.apps.rlp.mail;

import java.util.Date;

public class MailBuilder
{
	private String to;
	private String from;
	private String subject;
	private Date sendDate;
	private String content;
	
	public MailBuilder withTo(String input)
	{
		to = input;
		return this;
	}
	
	public MailBuilder withFrom(String input)
	{
		from = input;
		return this;
	}
	
	public MailBuilder withSubject(String input)
	{
		subject = input;
		return this;
	}
	
	public MailBuilder withSendDate(Date input)
	{
		sendDate = input;
		return this;
	}
	
	public MailBuilder withContent(String input)
	{
		content = input;
		return this;
	}
	
	public Mail build()
	{
		Mail output = new Mail();
		
		output.setTo(to);
		output.setFrom(from);
		output.setSubject(subject);
		output.setSendDate(sendDate);
		output.setContent(content);
		
		return output;
	}
}