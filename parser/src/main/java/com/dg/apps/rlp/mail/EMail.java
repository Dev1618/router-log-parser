package com.dg.apps.rlp.mail;

import java.util.Collection;

import com.dg.apps.rlp.mail.impl.GMailConnector;

public class EMail
{
	private static final MailConnector gmailConnector = new GMailConnector();
	
	public static final Collection<Mail> retrieve(MailServiceType mailServiceTypeInput, MailAuthentication mailAuthenticationInput) throws Exception
	{
		switch(mailServiceTypeInput)
		{
			case GMAIL:
				return gmailConnector.retrieve(mailAuthenticationInput);
			default:
				throw new Exception("no mail connector implementation");
		}
	}
}