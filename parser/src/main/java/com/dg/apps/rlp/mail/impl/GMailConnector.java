package com.dg.apps.rlp.mail.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;

import com.dg.apps.rlp.mail.Mail;
import com.dg.apps.rlp.mail.MailAuthentication;
import com.dg.apps.rlp.mail.MailBuilder;
import com.dg.apps.rlp.mail.MailConnector;
import com.sun.mail.imap.IMAPFolder;

public class GMailConnector extends MailConnector
{
	@Override
	public Collection<Mail> retrieve(final MailAuthentication authentication) throws MessagingException, IOException
	{
		Collection<Mail> output = new LinkedList<Mail>();
		IMAPFolder emailFolder = null;
		Store store = null;
		
		try
		{
			Properties properties = System.getProperties();
			properties.setProperty("mail.store.protocol", "imaps");
			
			Session emailSession = Session.getDefaultInstance(properties, null);
			store = emailSession.getStore("imaps");
			store.connect("imap.googlemail.com", authentication.getUsername(), authentication.getPassword());
			emailFolder = (IMAPFolder) store.getFolder("INBOX");
			emailFolder.open(Folder.READ_ONLY);
			
			Message[] messages = emailFolder.getMessages();
			System.out.println("there are [" + messages.length + "] messages in INBOX.");
			
			for(Message message : messages)
			{
				if("NETGEAR WNDR3700 Log".equals(message.getSubject()) && "text/plain".equals(message.getContentType().toLowerCase()))
				{
					System.out.println("creating new Mail instance for [" + message.getSubject() + "].");
					Mail mail = new MailBuilder()
							.withSubject(message.getSubject())
							.withContent(message.getContent().toString())
							.withSendDate(message.getSentDate())
							.build();
					output.add(mail);
				}
			}
		}
		finally
		{
			if(emailFolder != null)
			{
				emailFolder.close(false);
			}
			if(store != null)
			{
				store.close();
			}
		}
		
		return output;
	}
}