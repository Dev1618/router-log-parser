package com.dg.apps.rlp.mail;

import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class Mail
{
	@Getter
	@Setter
	private String to;
	
	@Getter
	@Setter
	private String from;
	
	@Getter
	@Setter
	private String subject;
	
	@Getter
	@Setter
	private Date sendDate;
	
	@Getter
	@Setter
	private String content;
	
	@Getter
	@Setter
	private boolean processed = false;
	
	public List<String> getContentByLines()
	{
		try
		{
			return Arrays.asList(content.split("\n"));
		}
		catch(Exception ex)
		{
			return new LinkedList<String>();
		}
	}
}