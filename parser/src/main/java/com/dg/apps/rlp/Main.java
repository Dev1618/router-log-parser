package com.dg.apps.rlp;

import java.util.Collection;

import com.dg.apps.rlp.db.impl.LogLinesTable;
import com.dg.apps.rlp.mail.EMail;
import com.dg.apps.rlp.mail.Mail;
import com.dg.apps.rlp.mail.MailAuthentication;
import com.dg.apps.rlp.mail.MailServiceType;

public class Main
{
	public static void main(String []args) throws Exception
	{
		
		updateUnprocessedLogLines();
		
		System.out.println("main(): done.");
	}
	
	private static void updateUnprocessedLogLines() throws Exception
	{
		System.out.println("updating unprocessed log lines...");
		
		Collection<Mail> emails = EMail.retrieve(MailServiceType.GMAIL, new MailAuthentication("dglogs48879@gmail.com","FAIL"));
		System.out.println("read [" + emails.size() + "] emails.");
		
		LogLinesTable unprocessedDatabase = new LogLinesTable();
		if(!unprocessedDatabase.exists())
		{
			unprocessedDatabase.create();
			System.out.println("created unprocessed db.");
		}
		else
		{
			System.out.println("unprocessed db already exists.");
		}
		
		for(Mail mail : emails)
		{
			try
			{
				unprocessedDatabase.insert(mail);
			}
			catch(Exception ex)
			{
				System.out.println("failed to insert mail [" + mail.getSendDate() + "] - " + ex.getClass().getSimpleName() + ": " + ex.getMessage());
			}
		}
	}
}





