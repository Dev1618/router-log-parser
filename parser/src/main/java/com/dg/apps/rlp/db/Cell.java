package com.dg.apps.rlp.db;

import lombok.Getter;
import lombok.Setter;

public class Cell<K,V>
{
	@Getter
	@Setter
	private K key;
	
	@Getter
	@Setter
	private V value;
	
	public Cell(K keyInput, V valueInput)
	{
		key = keyInput;
		value = valueInput;
	}
}