package com.dg.apps.rlp.mail;

import java.io.IOException;
import java.util.Collection;

import javax.mail.MessagingException;

public abstract class MailConnector
{
	public abstract Collection<Mail> retrieve(MailAuthentication mailAuthentication) throws MessagingException, IOException;
}