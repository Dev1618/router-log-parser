package com.dg.apps.rlp.parse;

import lombok.Getter;
import lombok.Setter;

/**
 * A single line from a {@link Mail} object's
 * content field.
 * 
 * @author david
 */
public class LogLine
{
	@Getter
	@Setter
	private String content;
	
	@Getter
	@Setter
	private long sendDate;
	
	@Getter
	@Setter
	private boolean processed;
}