package com.dg.apps.rlp.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public abstract class Database<T>
{
	protected static final String SQLLITE_JDBC_CLASS_NAME = "org.sqlite.JDBC";
	protected static final String DATABASE_URL = "jdbc:sqlite:";
	
	protected final String filePathSpec;
	
	protected Database(String filePathSpecInput)
	{
		filePathSpec = filePathSpecInput;
	}
	
	public abstract int create() throws ClassNotFoundException, SQLException;
	public abstract boolean exists() throws ClassNotFoundException, SQLException;
	public abstract int insert(T input) throws ClassNotFoundException, SQLException;
	
	protected int write(String sql) throws ClassNotFoundException, SQLException
	{
		Connection connection = null;
		Statement statement = null;
		
		try
		{
			Class.forName(SQLLITE_JDBC_CLASS_NAME);
			connection = DriverManager.getConnection(DATABASE_URL + filePathSpec);
			connection.setAutoCommit(false);
			statement = connection.createStatement();
			int result = statement.executeUpdate(sql);
			connection.commit();
			System.out.println("wrote [" + result + "] to [" + filePathSpec + "].");
			return result;
		}
		catch(ClassNotFoundException | SQLException ex)
		{
			rollback(connection);
			throw ex;
		}
		finally
		{
			close(statement);
			close(connection);
		}
	}
	
	protected int write(T input, String sql) throws ClassNotFoundException, SQLException
	{
		Connection connection = null;
		PreparedStatement statement = null;
		
		try
		{
			Class.forName(SQLLITE_JDBC_CLASS_NAME);
			connection = DriverManager.getConnection(DATABASE_URL + filePathSpec);
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql);
			int result = statement.executeUpdate(sql);
			connection.commit();
			System.out.println("wrote [" + result + "] to [" + filePathSpec + "].");
			return result;
		}
		catch(ClassNotFoundException | SQLException ex)
		{
			rollback(connection);
			throw ex;
		}
		finally
		{
			close(statement);
			close(connection);
		}
	}
	
	protected Collection<Collection<Cell<String,String>>> select(String sql) throws ClassNotFoundException, SQLException
	{
		Collection<Collection<Cell<String,String>>> output = new LinkedList<Collection<Cell<String,String>>>();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		try
		{
			Class.forName(SQLLITE_JDBC_CLASS_NAME);
			connection = DriverManager.getConnection(DATABASE_URL + filePathSpec);
			connection.setAutoCommit(false);
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sql);
			
			Map<Integer,String> map = mapColumnNames(resultSet);
			
			while(resultSet.next())
			{
				Collection<Cell<String,String>> row = new LinkedList<Cell<String,String>>();
				for(String columnName : map.values())
				{
					Cell<String,String> cell = new Cell<String,String>(columnName, resultSet.getString(columnName));
					row.add(cell);
				}
				output.add(row);
			}
			
			return output;
		}
		catch(ClassNotFoundException | SQLException ex)
		{
			rollback(connection);
			throw ex;
		}
		finally
		{
			close(resultSet);
			close(statement);
			close(connection);
		}
	}
	
	protected void close(AutoCloseable closeable)
	{
		if(closeable != null)
		{
			try
			{
				closeable.close();
			}
			catch (Exception e)
			{
				//
			}
		}
	}
	
	protected void rollback(Connection connection)
	{
		if(connection != null)
		{
			try
			{
				connection.rollback();
			}
			catch(SQLException ex)
			{
				//
			}
		}
	}
	
	protected Map<Integer,String> mapColumnNames(ResultSet resultSet) throws SQLException
	{
		Map<Integer,String> map = new HashMap<Integer,String>();
		for(int c = 1; c < resultSet.getMetaData().getColumnCount(); c++)
		{
			String columnName = resultSet.getMetaData().getColumnName(c);
			map.put(c, columnName);
		}
		return map;
	}
}