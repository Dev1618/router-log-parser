package com.dg.apps.rlp.mail;

import lombok.Getter;
import lombok.Setter;

public class MailAuthentication
{
	@Getter
	@Setter
	private String username;
	
	@Getter
	@Setter
	private String password;
	
	public MailAuthentication(String usernameInput, String passwordInput)
	{
		username = usernameInput;
		password = passwordInput;
	}
}