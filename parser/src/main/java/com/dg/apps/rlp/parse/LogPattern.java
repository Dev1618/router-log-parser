package com.dg.apps.rlp.parse;

import java.util.regex.Pattern;

public enum LogPattern
{
	/* dos attacks */
	DOS_ATTACK_TCP_UDP_CHARGEN(Pattern.compile("^\\[DoS Attack\\: TCP\\/UDP Chargen\\] from source\\: .*, port .*$")),
	DOS_ATTACK_ACK_SCAN(Pattern.compile("^\\[DoS Attack\\: ACK Scan\\] from source\\: .*, .*$")),
	DOS_ATTACK_RST_SCAN(Pattern.compile("^\\[DoS Attack\\: RST Scan\\] from source\\: .*, .*$")),
	DOS_ATTACK_TCP_UDP_ECHO(Pattern.compile("^\\[DoS Attack\\: TCP\\/UDP Echo\\] from source\\: .*$")),
	
	/* standard router ops */
	TIME_SYNCHRONIZED(Pattern.compile("^\\[Time synchronized with NTP server\\].*$")),
	DHCP_IP_ASSIGNED(Pattern.compile("^\\[DHCP IP\\: .*\\] to MAC address.*,.*$")),
	INTERNET_CONNECTED(Pattern.compile("^\\[Internet connected\\] IP address\\: .*,.*$")),
	INITIALIZED(Pattern.compile("^\\[Initialized, firmware version\\: .*\\] .*$")),
	ADMIN_LOGIN(Pattern.compile("^\\[admin login\\] from source .*, .*$")),
	
	/* logging */
	EMAIL_SENT(Pattern.compile("^\\[email sent to\\: .*\\].*$")),
	EMAIL_FAILED(Pattern.compile("^\\[email failed\\] SMTPclient\\: unexpected reply\\:.*$")),
	
	/* unknown or blank */
	BLANK(Pattern.compile("^$")),
	UNDEFINED(null),
	;
	
	
	private Pattern pattern;
	
	private LogPattern(Pattern patternInput)
	{
		pattern = patternInput;
	}
	
	public Pattern getPattern()
	{
		return pattern;
	}
	
	public static LogPattern of(String input)
	{
		if (input == null)
		{
			return UNDEFINED;
		}
		
		for (LogPattern pattern : LogPattern.values())
		{
			if (pattern.pattern != null && pattern.pattern.matcher(input.trim()).matches())
			{
				return pattern;
			}
		}
		
		return UNDEFINED;
	}
}