# router-log-parser

parse and process router logs


## steps/projects
* reader
  * read emails from server
  * commit emails (line by line) to local database
  * mark all database rows as unprocessed
* processor
  * read lines from database
  * for each "line" (db row):
    * process line pattern and write to "parsed" tables; mark "parse" as processed=true
    * retrieve ip address info; mark "enrich" as processed=true
* analyzer
  * read "parsed" tables
  * calculate occurrence and other statistics, and print


## technologies
http://ip-api.com - ip lookup
sqlite - store ips we've already looked up so we dont look them up repeatedly



maven/jdk1.8 - java and build
